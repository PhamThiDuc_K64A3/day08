<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <link rel="stylesheet" href="dangki.css">
</head>
<body>
    <?php
    session_start();
    $khoa = $tukhoa ="";
    if (!empty($_SESSION["khoa"])){
        $khoa = $_SESSION["khoa"];
    }
    if (!empty($_SESSION["tukhoa"])){
        $tukhoa = $_SESSION["tukhoa"];
    }
    $pkhoa=array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $_SESSION = $_POST;
        $khoa = $_SESSION["khoa"];
        $tukhoa = $_SESSION["tukhoa"];
    }
    ?>
    <form method="POST" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="name">
            <label class="lab">Khoa </label>
                <select name="khoa" id="khoa" class="ip khoa search_khoa">
                    <option value=""></option>
                    <?php
                    foreach ($pkhoa as $key => $value) {
                        echo "\t<option value=\"{$value}\"";
                        if ($khoa == $value){
                            echo "selected";
                        }
                        echo ">{$value}</option>\n";
                    };
                    ?>
                </select>
        </div>
        <div class="name">
            <label class="lab">Từ khoá</label>
            <input class="inp" type="text" name="tukhoa" id="tukhoa" value="<?php echo $tukhoa?>" />
        </div>
        <div class="btn_ser">
            <input type="button" id="remove" class="dki" value="Xoá">
            <!-- <input type="button" id="search" class="dki" value="Tìm kiếm"> -->
            <button id="search"class="dki submit">Tìm kiếm</button>
        </div>
        
        <p class="num">Số sinh viên tìm thấy: XXX</p>
        <a href="./dangki.php"><input class="dki add" type="button" value="Thêm">
        </a>
        <table>
            <tr>
                <th class="col1"><p>No</p></th>
                <th class="col2"><p>Tên sinh viên</p></th>
                <th class="col3"><p>Khoa</p></th>
                <th class="col4"><p>Action</p></th>
            </tr>
            <tr>
                <td class="col1"><p>1</p></td>
                <td class="col2"><p>Nguyễn Văn A</p></td>
                <td class="col3"><p>Khoa học máy tính</p></td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>
            <tr>
                <td class="col1"><p>2</p></td>
                <td class="col2"><p>Trần Thị B</p></td>
                <td class="col3"><p>Khoa học máy tính</p></td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>
            <tr>
                <td class="col1"><p>3</p></td>
                <td class="col2"><p>Nguyễn Hoàng C</p></td>
                <td class="col2"><p>Khoa học vật liệu</p></td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>
            <tr>
                <td class="col1"><p>4</p></td>
                <td class="col2"><p>Đinh Quang D</p></td>
                <td class="col3"><p>Khoa học vật liệu</p></td>
                <td class="col4"><button>Xoá</button><button>Sửa</button></td>
            </tr>
        </table>
    </form>
    <script>
        $(document).ready(function(){
            $("#remove").click(function(){
                localStorage.clear();
                $("#khoa").val('');
                $("#tukhoa").val('');
            });
        
        });
    </script>
</body>
</html>